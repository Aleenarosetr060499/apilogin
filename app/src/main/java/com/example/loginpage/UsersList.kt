package com.example.loginpage

import com.google.gson.annotations.SerializedName
import java.util.*

data class UsersList (val status: String,
                        val message: String,
                        val date: Users)