package com.example.loginpage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressBarbaseactivity.visibility = View.GONE

        LoginbtnLoginbutton.setOnClickListener {
            val  email = EmailLogineditText.text.toString().trim()
            val password = PasswordLogineditText.text.toString().trim()
             val user_type: Int = 1

            if (email.isEmpty()){
                EmailLogineditText.error = "Email required"
                EmailLogineditText.requestFocus()
                return@setOnClickListener

            }

            if (password.isEmpty()){
                PasswordLogineditText.error = "Password required"
                PasswordLogineditText.requestFocus()
                return@setOnClickListener

                         
            }

            showLoading()
            if (isNetworkConnected()){

            RetrofitClient.instance.login(email,password,user_type)
                    .enqueue(object : Callback<UsersList>{
//
                            override fun onResponse(call: Call<UsersList>, response: Response<UsersList>) {
                            if(response.body()?.status == "success"){

                                  //Toast.makeText(applicationContext,"welcome",Toast.LENGTH_SHORT).show()
                                startActivity(
                                    Intent(this@MainActivity,ProfileActivity::class.java)
                                )
                                hideLoading()
                            }

                            else if (response.body()?.status == "error"){
                                Toast.makeText(applicationContext,"some error occurred",Toast.LENGTH_SHORT).show()
                                hideLoading()
                            }
                            else{

                                   Toast.makeText(applicationContext,"failed",Toast.LENGTH_SHORT).show()
                                hideLoading()
                            }
                        }
                        override fun onFailure(call: Call<UsersList>, t: Throwable) {
                            Toast.makeText(applicationContext,t.message,Toast.LENGTH_SHORT).show()
                            hideLoading()
                        }
                    })

        }
            else{
                    hideLoading()
                Toast.makeText(this," check network connection",Toast.LENGTH_SHORT).show()

            }
        }



    }

    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)

    }

}
