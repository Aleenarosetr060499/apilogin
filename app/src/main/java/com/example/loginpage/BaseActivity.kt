package com.example.loginpage

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

open class BaseActivity : AppCompatActivity() {

    @SuppressLint("ServiceCast")
    fun isNetworkConnected() : Boolean {
        val connectivityManager=this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo?.isConnectedOrConnecting == true
    }

    fun showLoading() {
        progressBarbaseactivity?.visibility = View.VISIBLE
    }
    fun hideLoading() {
        progressBarbaseactivity?.visibility = View.GONE
    }


}
